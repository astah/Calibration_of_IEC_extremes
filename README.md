# Calibration_of_IEC_extremes

With these python scripts the Rational Calibration model of Gunner Larsen and Kurt Hansen is implemented, giving extreme gust amplitudes, extreme wind directional change and extreme wind shear. A terrain- and altitude-dependent constant C(z) is calibrated using measurements.

## Prerequisites

The scripts run on Python 3.7 and scipy 1.1.0.

## To install the scripts

Clone the repository to your local machine

## Gust_calibration.py:

This script contains the model of extreme gust with directional change (ECD), extreme wind directional change (EDC), extreme operating gust (EOG) and extreme wind shear (EWS).
The output is a number plots showing e.g. gust amplitude or directional change as function of wind speed.
Here the model is implemented for three different terrain types:
1. Forest (Ryningsnæs)
2. Flat/homogeneous (Høvsøre east sectors)
3. Offshore/coastal (Høvsøre west sectors) 

The input parameters are:  
Annual average wind speed, rotor diameter, turbulence intensity, hub height, C(z), sigma_v/sigma_u and turbulent length scale. 
Data from Ryningsnæs and Høvsøre is used to calculate the site specific turbulence intencity.
 
## Fit_to_tail.py:

With this script the parameter C(z) can be estimated from data.
The input data contains distributions (histograms) of normalized turbulence excursions from Høvsøre east sectors, Høvsøre west sectors and Ryningsnæs.
The script also gives ratios of sigma_v and sigma_u at the given sites. 

## funcs.py:
This is a set of functions called by the different scripts.

## NB 
The plots produced in Gust_calibration.py are not identical to the plots in the the original paper describing the model. This is because some changes have been implemented:
1. Site specific values are used for reference turbulence intensity.
2. The ratio of sigma_v/sigma_u has been implemented with site specific values.
