#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 11:00:59 2018

@author: astahannesdottir
"""
#%%
import numpy as np
from funcs import iec_pdf, isite, f_scale, ecd_excursions, edc_excursion, \
eog_excursion, ews_excursion
import matplotlib.pyplot as plt
import seaborn as sns

# Read in the data
file = 'hovsore_data/hosvore_U_dir_100m_2017.txt'
data = np.genfromtxt(file)  # Date, wsp, dir, sigma

file1 = 'ryningsnaes_data/ryningsnaes_U_dir_98m_2011.txt'
data1 = np.genfromtxt(file1)  # Date, wsp, dir, sigma

#%% Time span distribution
t = 50*365*24*60*60  # 50-year return period in seconds
u_bins = np.arange(3,27,1)

v_ave_west = 10   
v_ave_east = 8.5 
v_ave_ryn =  7.5
# Alternatively use the v_ave function:  vave(data, 270, sbin)

# The total time in each wind speed bin
t_west = iec_pdf(u_bins,v_ave_west,2)*t
t_east = iec_pdf(u_bins,v_ave_east,2)*t
t_ryn = iec_pdf(u_bins,v_ave_ryn,2)*t

#%% Input parameters

i_west = isite(data, 270, 30)     # Turbulence intensity at Høvsøre west
i_east = isite(data, 90, 30)      # Turbulence intensity at Høvsøre east
i_ryn = isite(data1, 270-145, 30)     # Turbulence intensity at Ryningsnæs
# Alternatively use i_ref according to iec turbulence classes

z = 100                     # Hub height
cz_west = 0.369             # 0.0013*z + 0.3026    Alternative linear model
cz_east = 0.351             # 0.0003*z + 0.3011
cz_ryn = 0.316

dr = 126                    # Rotor diameter

lu = 8.1*42                 # Turbulent length scale, u-component. For z > 60m
lv = 2.7*42                 # Turbulent length scale, v-component. For z > 60m

s_ratio_west = 0.71         # Ratio between sigma_v and sigma_u at Høvsøre west
s_ratio_east = 0.76         # Ratio between sigma_v and sigma_u at Høvsøre east
s_ratio_ryn = 0.91          # Ratio between sigma_v and sigma_u at Ryningsnæs

#%% ECD model implementation

m_u_west, m_v_west = ecd_excursions(u_bins,t_west,i_west,dr,cz_west,\
                                    s_ratio_west,lu,lv)
m_theta_west = np.arctan2(m_v_west,(u_bins + m_u_west))*180/np.pi

m_u_east, m_v_east = ecd_excursions(u_bins,t_east,i_east,dr,cz_east,\
                                    s_ratio_east,lu,lv)
m_theta_east = np.arctan2(m_v_east,(u_bins + m_u_east))*180/np.pi

m_u_ryn, m_v_ryn = ecd_excursions(u_bins,t_ryn,i_ryn,dr,cz_ryn,s_ratio_ryn,lu,lv)
m_theta_ryn = np.arctan2(m_v_ryn,(u_bins + m_u_ryn))*180/np.pi

# ECD model implementation with rise time scaling factor
tau = 10
mf_u_west = m_u_west*f_scale(tau,u_bins,lu)
mf_v_west = m_v_west*f_scale(tau,u_bins,lv)
mf_theta_west = np.arctan2(mf_v_west,(u_bins + mf_u_west))*180/np.pi

mf_u_east = m_u_east*f_scale(tau,u_bins,lu)
mf_v_east = m_v_east*f_scale(tau,u_bins,lv)
mf_theta_east = np.arctan2(mf_v_east,(u_bins + mf_u_east))*180/np.pi

mf_u_ryn = m_u_ryn*f_scale(tau,u_bins,lu)
mf_v_ryn = m_v_ryn*f_scale(tau,u_bins,lv)
mf_theta_ryn = np.arctan2(mf_v_ryn,(u_bins + mf_u_ryn))*180/np.pi

#%% EDC model implementation

m_edc_west = edc_excursion(u_bins,t_west,i_west,dr,cz_west,s_ratio_west,lv)
edc_theta_west = np.arctan2(m_edc_west,u_bins)*180/np.pi

m_edc_east = edc_excursion(u_bins,t_east,i_east,dr,cz_east,s_ratio_east,lv)
edc_theta_east = np.arctan2(m_edc_east,u_bins)*180/np.pi

m_edc_ryn = edc_excursion(u_bins,t_ryn,i_ryn,dr,cz_ryn,s_ratio_ryn,lv)
edc_theta_ryn = np.arctan2(m_edc_ryn,u_bins)*180/np.pi
# Model implementation with rise time scaling factor
tau = 6
mf_edc_west = m_edc_west*f_scale(tau,u_bins,lv)
edc_f_theta_west = np.arctan2(mf_edc_west,u_bins)*180/np.pi

mf_edc_east = m_edc_east*f_scale(tau,u_bins,lv)
edc_f_theta_east = np.arctan2(mf_edc_east,u_bins)*180/np.pi

mf_edc_ryn = m_edc_ryn*f_scale(tau,u_bins,lv)
edc_f_theta_ryn = np.arctan2(mf_edc_ryn,u_bins)*180/np.pi

#%% EOG model implementation

m_eog_west = eog_excursion(u_bins,t_west,i_west,dr,cz_west,lu)
m_eog_east = eog_excursion(u_bins,t_east,i_east,dr,cz_east,lu)
m_eog_ryn = eog_excursion(u_bins,t_ryn,i_ryn,dr,cz_ryn,lu)

# Model implementation with rise time scaling factor
tau = 5.25
mf_eog_west = m_eog_west*f_scale(tau,u_bins,lu)
mf_eog_east = m_eog_east*f_scale(tau,u_bins,lu)
mf_eog_ryn = m_eog_ryn*f_scale(tau,u_bins,lu)

#%% EWS model implementation

m_ews_west = ews_excursion(u_bins,t_west,i_west,dr,cz_west,lu)
m_ews_east = ews_excursion(u_bins,t_east,i_east,dr,cz_east,lu)
m_ews_ryn = ews_excursion(u_bins,t_ryn,i_ryn,dr,cz_ryn,lu)

# Model implementation with rise time scaling factor
tau = 6
mf_ews_west = m_ews_west*f_scale(tau,u_bins,lu)
mf_ews_east = m_ews_east*f_scale(tau,u_bins,lu)
mf_ews_ryn = m_ews_ryn*f_scale(tau,u_bins,lu)

#%% Plots
plt.close('all')
sns.set(style="whitegrid", color_codes=True)

plt.figure(figsize=(12,4.4))
plt.subplot(1,2,1)
plt.plot(u_bins, m_u_west,'*-', label='Høvsøre West')
plt.plot(u_bins, m_u_east,'v-', label='Høvsøre East')
plt.plot(u_bins, m_u_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('U-gust [m/s]')
plt.legend(loc='best')
plt.subplot(1,2,2)
plt.plot(u_bins, m_theta_west,'*-', label='Høvsøre West')
plt.plot(u_bins, m_theta_east,'v-', label='Høvsøre East')
plt.plot(u_bins, m_theta_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Direction change [deg]')
plt.legend(loc='best')
plt.suptitle('ECD')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

plt.figure(figsize=(12,4.4))
plt.subplot(1,2,1)
plt.plot(u_bins, mf_u_west,'*-', label='Høvsøre West')
plt.plot(u_bins, mf_u_east,'v-', label='Høvsøre East')
plt.plot(u_bins, mf_u_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('U-gust [m/s]')
plt.legend(loc='best')
plt.subplot(1,2,2)
plt.plot(u_bins, mf_theta_west,'*-', label='Høvsøre West')
plt.plot(u_bins, mf_theta_east,'v-', label='Høvsøre East')
plt.plot(u_bins, mf_theta_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Direction change [deg]')
plt.legend(loc='best')
plt.suptitle('ECD with rise time')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

plt.figure(figsize=(12,4.4))
plt.subplot(1,2,1)
plt.title('EDC')
plt.plot(u_bins, edc_theta_west,'*-', label='Høvsøre West')
plt.plot(u_bins, edc_theta_east,'v-', label='Høvsøre East')
plt.plot(u_bins, edc_theta_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Direction change [deg]')
plt.legend(loc='best')
plt.subplot(1,2,2)
plt.title('EDC with riese time')
plt.plot(u_bins, edc_f_theta_west,'*-', label='Høvsøre West')
plt.plot(u_bins, edc_f_theta_east,'v-', label='Høvsøre East')
plt.plot(u_bins, edc_f_theta_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Direction change [deg]')
plt.legend(loc='best')
plt.tight_layout()

plt.figure(figsize=(12,4.4))
plt.subplot(1,2,1)
plt.title('EOG')
plt.plot(u_bins, m_eog_west,'*-', label='Høvsøre West')
plt.plot(u_bins, m_eog_east,'v-', label='Høvsøre East')
plt.plot(u_bins, m_eog_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Gust [m/s]')
plt.legend(loc='best')
plt.subplot(1,2,2)
plt.title('EOG with riese time')
plt.plot(u_bins, mf_eog_west,'*-', label='Høvsøre West')
plt.plot(u_bins, mf_eog_east,'v-', label='Høvsøre East')
plt.plot(u_bins, mf_eog_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Gust [m/s]')
plt.legend(loc='best')
plt.tight_layout()

plt.figure(figsize=(12,4.4))
plt.subplot(1,2,1)
plt.title('EWS')
plt.plot(u_bins, m_ews_west,'*-', label='Høvsøre West')
plt.plot(u_bins, m_ews_east,'v-', label='Høvsøre East')
plt.plot(u_bins, m_ews_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Wind speed difference [m/s]')
plt.legend(loc='best')
plt.subplot(1,2,2)
plt.title('EWS with riese time')
plt.plot(u_bins, mf_ews_west,'*-', label='Høvsøre West')
plt.plot(u_bins, mf_ews_east,'v-', label='Høvsøre East')
plt.plot(u_bins, mf_ews_ryn, 'o-', label='Ryningsnæs West')
plt.xlabel('U [m/s]')
plt.ylabel('Wind speed difference [m/s]')
plt.legend(loc='best')
plt.tight_layout()
# %%
