#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 13:25:46 2018

@author: astahannesdottir
"""
import numpy as np
from scipy.special import gamma, hyp1f2
# from mpmath import hyp1f2
import scipy.integrate as integrate
#==============================================================================
def rms(y, yfit):
    return np.sqrt(np.mean((y-yfit)**2))
#==============================================================================
def vave(data,sector,sbin):
    # Remove nans
    mask_nans = np.isnan(data)
    ind_nans = np.unique(np.nonzero(mask_nans)[0])
    data = np.delete(data, ind_nans, axis=0)
    # Choose a certain sector
    sec = [sector-sbin/2,sector+sbin/2]
    mask_sec = (data[:,2]>sec[0]) & (data[:,2]<sec[1])
    u_mean = data[mask_sec,1].mean()
    return u_mean
#==============================================================================
def isite(data,sector,sbin):
    # Remove nans
    mask_nans = np.isnan(data)
    ind_nans = np.unique(np.nonzero(mask_nans)[0])
    data = np.delete(data, ind_nans, axis=0)
    # Choose a certain sector
    sec = [sector-sbin/2,sector+sbin/2]
    mask_sec = (data[:,2]>sec[0]) & (data[:,2]<sec[1])
    mask_u = (data[mask_sec,1] > 14) & (data[mask_sec,1] < 16)
    if not any(mask_u):
         mask_u = (data[mask_sec,1] > 8) & (data[mask_sec,1] < 12)    
    i = np.mean(data[mask_sec,3][mask_u])/\
            (0.75*np.mean(data[mask_sec,1][mask_u])+3.8)
    return i
#==============================================================================
def iec_pdf(ubins,uave,k):
    c = uave/gamma(1+1/k)
    pdf = k/c*(ubins/c)**(k-1)*np.exp(-(ubins/c)**k)
    return pdf
#==============================================================================
def m_2(ubins,l,fc):
    m = ubins**2/54/l**2*(0.75*(1+6*fc*l/ubins)**(4/3) - \
        6*(1+6*fc*l/ubins)**(1/3) - 2/3*(1+6*fc*l/ubins)**(-2/3) + 27/4)
    return m

def m_4(ubins,l,fc):
    m = ubins**4/1944/l**4*(0.3*(1+6*fc*l/ubins)**(10/3) - \
        12/7*(1+6*fc*l/ubins)**(7/3) + 9/2*(1+6*fc*l/ubins)**(4/3) - \
        12*(1+6*fc*l/ubins)**(1/3) - 3/2*(1+6*fc*l/ubins)**(-2/3) + 729/70)
    return m

#==============================================================================
# The extreme excursions of a joint event (ECD)

def ecd_excursions(ubins,tbins,iref,dr,c,sratio,lu,lv):
    """
        The excursion function takes as input:
        - Wind speed in bins (ubins)
        - Total time in each wind speed bin (tbins)
        - Turbulence intensity (iref)
        - Rotor diameter (dr)
        - Terrain- and height constant (c)
        - Ratio between sigma_v and sigma_u (sratio)
        - Turbulence length scale u-component
        - Turbulence length scale v-component
        """
    sigu = iref*(0.75*ubins + 5.6)
    sigv = sratio*sigu
    sigu_calib = np.sqrt(1-(1+6*lu/2/dr)**(-2/3))
    sigv_calib = np.sqrt(1-(1+6*lv/2/dr)**(-2/3))
    fc = ubins/2/dr
    m_u2 = m_2(ubins,lu,fc)
    m_v2 = m_2(ubins,lv,fc)
    m_u4 = m_4(ubins,lu,fc)
    m_v4 = m_4(ubins,lv,fc)
    m_u = 2*c*sigu*sigu_calib*np.log(tbins*np.sqrt(m_v4*m_u2/m_v2))
    m_v = 2*c*sigv*sigv_calib*np.log(tbins*np.sqrt(m_u4*m_v2/m_u2))
    return m_u, m_v
#==============================================================================
def edc_excursion(ubins,tbins,iref,dr,c,sratio,lv):
    """
        The excursion function takes as input:
        - Wind speed in bins (ubins)
        - Total time in each wind speed bin (tbins)
        - Turbulence intensity (iref)
        - Rotor diameter (dr)
        - Terrain- and height constant (c)
        - Ratio between sigma_v and sigma_u (sratio)
        - Turbulence length scale v-component
        """
    sigu = iref*(0.75*ubins + 5.6)
    sigv = sratio*sigu
    sigv_calib = np.sqrt(1-(1+6*lv/2/dr)**(-2/3))
    fc = ubins/2/dr
    m_v2 = m_2(ubins,lv,fc)
    m_v4 = m_4(ubins,lv,fc)
    kappa = np.exp(-1/c)*np.sqrt(m_v2**3/m_v4)
    m_edc = 2*c*sigv*sigv_calib*np.log(tbins*kappa)
    return m_edc
#==============================================================================

def eog_excursion(ubins,tbins,iref,dr,c,lu):
    """
    The excursion function takes as input: 
        - Wind speed in bins (ubins) 
        - Total time in each wind speed bin (tbins)
        - Turbulence intensity (iref)
        - Rotor diameter (dr)
        - Terrain- and height constant (c)
        - Turbulence length scale u-component
    """
    sigu = iref*(0.75*ubins + 5.6)
    sig_calib = np.sqrt(1-(1+6*lu/2/dr)**(-2/3))
    fc = ubins/2/dr
    m_u2 = m_2(ubins,lu,fc)
    m_u4 = m_4(ubins,lu,fc)
    kappa = np.exp(-1/c)*np.sqrt(m_u2**3/m_u4)
    m_eog = 2*c*sigu*sig_calib*np.log(tbins*kappa)
    return m_eog

#==============================================================================

def ews_excursion(ubins,tbins,iref,dr,c,lu):
    """
    The excursion function takes as input: 
        - Wind speed in bins (ubins) 
        - Total time in each wind speed bin (tbins)
        - Turbulence intensity (iref)
        - Rotor diameter (dr)
        - Terrain- and height constant (c)
        - Turbulence length scale u-component
    """
    def integrand(x, d, l):
        return np.exp( -12*np.sqrt(x**2 + (0.12*d/l)**2))*(1+6*x*l/d)**(-5/3)

    I = integrate.quad(integrand, 0, 1, args=(dr,lu))
    rho = 4/dr*lu/(1-(1+6*lu/2/dr)**(-2/3))*I[0]
    sigu = iref*(0.75*ubins + 5.6)
    sig_calib = np.sqrt(1-(1+6*lu/2/dr)**(-2/3))
    sig_eff = np.sqrt(2*(1-rho))*sig_calib*sigu
    fc = ubins/dr
    m_u2 = m_2(ubins,lu,fc)
    m_u4 = m_4(ubins,lu,fc)
    kappa = np.exp(-1/c)*np.sqrt(m_u2**3/m_u4)
    m_ews = 2*c*sig_eff*np.log(tbins*kappa)
    return m_ews
#==============================================================================
# The scaling factor of the gust amplitues as function of rise time
def f_scale(tau,ubins,l):
    term1 = np.pi**2*tau**2*ubins**2/(36*l**2)
    term2 = np.pi*ubins*tau/(3*l)
    f = 1 - 2/(27*gamma(5/3))*(9*gamma(2/3)*hyp1f2(1,1/6,2/3,-term1)[0] + \
            (3*np.pi**5*ubins**2/l**2)**(1/3)*tau**(2/3)*\
            (3*np.sin(term2) - 3**(1/2)*np.cos(term2)))
    return f
    
