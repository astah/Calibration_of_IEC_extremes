#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 12:10:06 2018

@author: astahannesdottir
"""

import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import seaborn as sns
from funcs import rms

#file0 = np.load('hovsore_data/hovsore_hist_u_east.npz')
#file1 = np.load('hovsore_data/hovsore_hist_u_west.npz')
#file2 = np.load('ryningsnaes_data/ryningsnaes_hist_u_west.npz')

#file0 = np.load('hovsore_data/hovsore_hist_u_detrend_east.npz')
#file1 = np.load('hovsore_data/hovsore_hist_u_detrend_west.npz')
#file2 = np.load('ryningsnaes_data/ryningsnaes_hist_u_detrend_west.npz')

file0 = np.load('hovsore_data/hovsore_hist_u_highpass_east.npz')
file1 = np.load('hovsore_data/hovsore_hist_u_highpass_west.npz')
file2 = np.load('ryningsnaes_data/ryningsnaes_hist_u_highpass_west.npz')


z_hov = [20, 40, 60, 80, 100]  # the measurement heights in Høvsøre
z_ryn = [40, 59, 80, 98, 120, 137.7] # the measurement heights in Ryningsnæs

u_centers = file0.items()[0][1]

hist_u0 = file0.items()[1][1]
hist_norm_u0 = hist_u0/np.sum(hist_u0, axis=0)/0.01
hist_v0 = file0.items()[2][1]
hist_norm_v0 = hist_v0/np.sum(hist_v0, axis=0)/0.01

hist_u1 = file1.items()[1][1]
hist_norm_u1 = hist_u1/np.sum(hist_u1, axis=0)/0.01
hist_v1 = file1.items()[2][1]
hist_norm_v1 = hist_v1/np.sum(hist_v1, axis=0)/0.01

hist_u2 = file2.items()[1][1]
hist_norm_u2 = hist_u2/np.sum(hist_u2, axis=0)/0.01
hist_v2 = file2.items()[2][1]
hist_norm_v2 = hist_v2/np.sum(hist_v2, axis=0)/0.01

N0 = np.sum(hist_u0, axis=0)
N1 = np.sum(hist_u1, axis=0)
N2 = np.sum(hist_u2, axis=0)

sigma_est0 = np.sqrt(np.sum(u_centers**2*hist_u0.transpose(),axis=1)/N0)
sigma_est1 = np.sqrt(np.sum(u_centers**2*hist_u1.transpose(),axis=1)/N1)
sigma_est2 = np.sqrt(np.sum(u_centers**2*hist_u2.transpose(),axis=1)/N2)

sigma_est0v = np.sqrt(np.sum(u_centers**2*hist_v0.transpose(),axis=1)/N0)
sigma_est1v = np.sqrt(np.sum(u_centers**2*hist_v1.transpose(),axis=1)/N1)
sigma_est2v = np.sqrt(np.sum(u_centers**2*hist_v2.transpose(),axis=1)/N2)

print('Høvsøre east 100 m, sigma_v/sigma_u: ', sigma_est0v[4]/sigma_est0[4] )
print('Høvsøre west 100 m, sigma_v/sigma_u: ', sigma_est1v[4]/sigma_est1[4] )
print('Ryningsnæs 98 m, sigma_v/sigma_u: ', sigma_est2v[3]/sigma_est2[3] )
#%%
""" 
Fit the Gamma distribution function to the tail of the distribution of the
turbulence components to get the C-parameter
Note Gunners function is equal to 1/2 the gamma: f_gunner = 1/2 f_gamma
"""
def f_g(x,sig,c):
    result = 1/2/np.sqrt(2*np.pi*c*sig*x)*np.exp(-x/2/c/sig)
    return result

def lnf_g(x,sig,c):
    result = -x/(2*c*sig) - np.log(2*np.sqrt(2*np.pi*c*sig*x))
    return result

c_0 = np.zeros(len(N0))
c_1 = np.zeros(len(N1))
c_2 = np.zeros(len(N2))


# Fit the log of the function to the log of the tail data
#for i in range(len(N0)):
#    xval = u_centers[(u_centers>4*sigma_est0[i]) & (hist_norm_u0[:,i]>0)]
#    data_samp = np.log(hist_norm_u0[(u_centers>4*sigma_est0[i])& (hist_norm_u0[:,i]>0), i])
#    p0 = sigma_est0[i], 0.3
#    popt, pcov = curve_fit(lnf_g, xval, data_samp, p0=p0, bounds=([sigma_est0[i]-0.001, 0.2],[sigma_est0[i]+0.001, 0.6]))
#    c_0[i] = popt[1]
#print('rms error in last fit:', rms(np.exp(data_samp), f_g(xval,*popt)))

#for i in range(len(N1)):
#    xval = u_centers[(u_centers>4*sigma_est1[i]) & (hist_norm_u1[:,i]>0)]
#    data_samp = np.log(hist_norm_u1[(u_centers>4*sigma_est1[i])& (hist_norm_u1[:,i]>0), i])
#    p0 = sigma_est1[i], 0.3
#    popt, pcov = curve_fit(lnf_g, xval, data_samp, p0=p0, bounds=([sigma_est1[i]-0.001, 0.2],[sigma_est1[i]+0.001, 0.6]))
#    c_1[i] = popt[1]
#print('rms error in last fit:', rms(np.exp(data_samp), f_g(xval,*popt)))

for i in range(len(N2)):
    xval = u_centers[(u_centers>4*sigma_est2[i]) & (hist_norm_u2[:,i]>0)]
    data_samp = np.log(hist_norm_u2[(u_centers>4*sigma_est2[i])& (hist_norm_u2[:,i]>0), i])
    p0 = sigma_est2[i], 0.2
    popt, pcov = curve_fit(lnf_g, xval, data_samp, p0=p0, bounds=([sigma_est2[i]-0.01, 0.1],[sigma_est2[i]+0.01, 0.4]))
    c_2[i] = popt[1]
print('rms error in last fit:', rms(np.exp(data_samp), f_g(xval,*popt)))

# Fit the gamma-function to the tail data   
for i in range(len(N0)):
    xval = u_centers[(u_centers>4*sigma_est0[i]) & (hist_norm_u0[:,i]>0)]
    data_samp = hist_norm_u0[(u_centers>4*sigma_est0[i])& (hist_norm_u0[:,i]>0), i]
    p0 = sigma_est0[i], 0.3
    popt, pcov = curve_fit(f_g, xval, data_samp, p0=p0, bounds=([sigma_est0[i]-0.001, 0.2],[sigma_est0[i]+0.001, 0.6]))
    c_0[i] = popt[1]
print('rms error in last fit:', rms(data_samp, f_g(xval,*popt)))

for i in range(len(N1)):
    xval = u_centers[(u_centers>4*sigma_est1[i]) & (hist_norm_u1[:,i]>0)]
    data_samp = hist_norm_u1[(u_centers>4*sigma_est1[i])& (hist_norm_u1[:,i]>0), i]
    p0 = sigma_est1[i], 0.3
    popt, pcov = curve_fit(f_g, xval, data_samp, p0=p0, bounds=([sigma_est1[i]-0.001, 0.2],[sigma_est1[i]+0.001, 0.6]))
    c_1[i] = popt[1]
print('rms error in last fit:', rms(data_samp, f_g(xval,*popt)))

#for i in range(len(N2)):
#    xval = u_centers[(u_centers>4*sigma_est2[i]) & (hist_norm_u2[:,i]>0)]
#    data_samp = hist_norm_u2[(u_centers>4*sigma_est2[i])& (hist_norm_u2[:,i]>0), i]
#    p0 = sigma_est2[i], 0.3
#    popt, pcov = curve_fit(f_g, xval, data_samp, p0=p0, bounds=([sigma_est2[i]-0.001, 0.2],[sigma_est2[i]+0.001, 0.6]))
#    c_2[i] = popt[1]
#print('rms error in last fit:', rms(data_samp, f_g(xval,*popt)))

print('Høvsøre east 100 m, c(z): ', c_0[4])
print('Høvsøre west 100 m, c(z): ', c_1[4])
print('Ryningsnæs 98 m, c(z): ', c_2[3])
#%% Plots
plt.close('all')
sns.set(style="whitegrid", color_codes=True)

plt.figure()
plt.plot(c_0, z_hov,'o-', label="Høvsøre East")
plt.plot(c_1, z_hov,'o-', label="Høvsøre West")
plt.plot(c_2, z_ryn,'o-', label="Ryningsnæs West")
plt.ylabel('z [m]')
plt.xlabel('c(z)')
plt.legend(loc='best')


fig=plt.figure(figsize=(6.4,7))
ax0=plt.subplot(4,1,1)
plt.title('Høvsøre East')
plt.semilogy(u_centers, hist_norm_u0[:,4], label="u'/U 100m")
plt.semilogy(u_centers, hist_norm_v0[:,4], label="v'/U 100m")
plt.semilogy(u_centers[(u_centers>4*sigma_est0[4]) & (hist_norm_u0[:,4]>0)],
             f_g(u_centers[(u_centers>4*sigma_est0[4]) & (hist_norm_u0[:,4]>0)]
             ,sigma_est0[4],c_0[4]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est0[4]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax1=plt.subplot(4,1,2,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u0[:,3], label="u'/U 80m")
plt.semilogy(u_centers, hist_norm_v0[:,3], label="v'/U 80m")
plt.semilogy(u_centers[(u_centers>4*sigma_est0[3]) & (hist_norm_u0[:,3]>0)],
             f_g(u_centers[(u_centers>4*sigma_est0[3]) & (hist_norm_u0[:,3]>0)]
             ,sigma_est0[3],c_0[3]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est0[3]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax2=plt.subplot(4,1,3,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u0[:,2], label="u'/U 60m")
plt.semilogy(u_centers, hist_norm_v0[:,2], label="v'/U 60m")
plt.semilogy(u_centers[(u_centers>4*sigma_est0[2]) & (hist_norm_u0[:,2]>0)],
             f_g(u_centers[(u_centers>4*sigma_est0[2]) & (hist_norm_u0[:,2]>0)]
             ,sigma_est0[2],c_0[2]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est0[2]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax3=plt.subplot(4,1,4,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u0[:,1], label="u'/U 40m")
plt.semilogy(u_centers, hist_norm_v0[:,1], label="v'/U 40m")
plt.semilogy(u_centers[(u_centers>4*sigma_est0[1]) & (hist_norm_u0[:,1]>0)],
             f_g(u_centers[(u_centers>4*sigma_est0[1]) & (hist_norm_u0[:,1]>0)]
             ,sigma_est0[1],c_0[1]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est0[1]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylim((3e-07,10))
plt.xlabel('(u-U)/U')
plt.ylabel('pdf')


plt.figure(figsize=(6.4,7))
ax0=plt.subplot(4,1,1)
plt.title('Høvsøre West')
plt.semilogy(u_centers, hist_norm_u1[:,4], label="u'/U 100m")
plt.semilogy(u_centers, hist_norm_v1[:,4], label="v'/U 100m")
plt.semilogy(u_centers[(u_centers>4*sigma_est1[4]) & (hist_norm_u1[:,4]>0)],
             f_g(u_centers[(u_centers>4*sigma_est1[4]) & (hist_norm_u1[:,4]>0)]
             ,sigma_est1[4],c_1[4]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est1[4]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax1=plt.subplot(4,1,2,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u1[:,3], label="u'/U 80m")
plt.semilogy(u_centers, hist_norm_v1[:,3], label="v'/U 80m")
plt.semilogy(u_centers[(u_centers>4*sigma_est1[3]) & (hist_norm_u1[:,3]>0)],
             f_g(u_centers[(u_centers>4*sigma_est1[3]) & (hist_norm_u1[:,3]>0)]
             ,sigma_est1[3],c_1[3]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est1[3]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax2=plt.subplot(4,1,3,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u1[:,2], label="u'/U 60m")
plt.semilogy(u_centers, hist_norm_v1[:,2], label="v'/U 60m")
plt.semilogy(u_centers[(u_centers>4*sigma_est1[2]) & (hist_norm_u1[:,2]>0)],
             f_g(u_centers[(u_centers>4*sigma_est1[2]) & (hist_norm_u1[:,2]>0)]
             ,sigma_est1[2],c_1[2]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est1[2]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax3=plt.subplot(4,1,4,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u1[:,1], label="u'/U 40m")
plt.semilogy(u_centers, hist_norm_v1[:,1], label="v'/U 40m")
plt.semilogy(u_centers[(u_centers>4*sigma_est1[1]) & (hist_norm_u1[:,1]>0)],
             f_g(u_centers[(u_centers>4*sigma_est1[1]) & (hist_norm_u1[:,1]>0)]
             ,sigma_est1[1],c_1[1]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est1[1]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylim((3e-07,10))
plt.xlabel('(u-U)/U')
plt.ylabel('pdf')


plt.figure(figsize=(6.4,7))
ax0=plt.subplot(4,1,1)
plt.title('Ryningsnæs West')
plt.semilogy(u_centers, hist_norm_u2[:,5], label="u'/U 138m")
plt.semilogy(u_centers, hist_norm_v2[:,5], label="v'/U 138m")
plt.semilogy(u_centers[(u_centers>4*sigma_est2[5]) & (hist_norm_u2[:,5]>0)],
             f_g(u_centers[(u_centers>4*sigma_est2[5]) & (hist_norm_u2[:,5]>0)]
             ,sigma_est2[5],c_2[5]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est2[5]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax1=plt.subplot(4,1,2,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u2[:,4], label="u'/U 120m")
plt.semilogy(u_centers, hist_norm_v2[:,4], label="v'/U 120m")
plt.semilogy(u_centers[(u_centers>4*sigma_est2[4]) & (hist_norm_u2[:,4]>0)],
             f_g(u_centers[(u_centers>4*sigma_est2[4]) & (hist_norm_u2[:,4]>0)]
             ,sigma_est2[3],c_2[3]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est2[4]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax2=plt.subplot(4,1,3,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u2[:,3], label="u'/U 98m")
plt.semilogy(u_centers, hist_norm_v2[:,3], label="v'/U 98m")
plt.semilogy(u_centers[(u_centers>4*sigma_est2[3]) & (hist_norm_u2[:,3]>0)],
             f_g(u_centers[(u_centers>4*sigma_est2[3]) & (hist_norm_u2[:,3]>0)]
             ,sigma_est2[3],c_2[3]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est2[3]), label='Gaussian fit')
plt.legend(loc='best')
plt.ylabel('pdf')
ax3=plt.subplot(4,1,4,sharex=ax0, sharey=ax0)
plt.semilogy(u_centers, hist_norm_u2[:,2], label="u'/U 80m")
plt.semilogy(u_centers, hist_norm_v2[:,2], label="v'/U 80m")
plt.semilogy(u_centers[(u_centers>4*sigma_est2[2]) & (hist_norm_u2[:,2]>0)],
             f_g(u_centers[(u_centers>4*sigma_est2[2]) & (hist_norm_u2[:,2]>0)]
             ,sigma_est2[2],c_2[2]), label='Tail fit')
plt.semilogy(u_centers, norm.pdf(u_centers, 0, sigma_est2[2]), label='Gaussian fit')
plt.legend(loc='best')
plt.xlabel('(u-U)/U')
plt.ylim((3e-07,10))
plt.ylabel('pdf')

